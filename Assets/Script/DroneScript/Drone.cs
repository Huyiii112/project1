﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Drone :MonoBehaviour
{
    public float m_delaytime = 3;
    [SerializeField]
    private Transform m_point;
    [SerializeField]
    private GameObject droneBullet1;
    private void Start()
    {
        StartCoroutine(DroneShotting());
    }
    private void Update()
    {
        
    }
    IEnumerator  DroneShotting()
    {
        GameObject droneBullet = ObjectPoolController.Instance.GetObject(ObjectType.LaserDrone);
        droneBullet.transform.position = m_point.position;
        yield return new WaitForSeconds(m_delaytime);
        StartCoroutine(DroneShotting());
    }
}
