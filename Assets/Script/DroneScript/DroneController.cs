﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneController : MonoBehaviour
{
    [SerializeField]
    private List<DroneSO> drones;

    public Drone drone1;
    public Drone drone2;
    // Start is called before the first frame update
    void Awake()
    {
        foreach (var drone in drones)
        {
            if (drone == GameData.drone1)
            {
                drone1 = drone.drone;
            }
            if (drone == GameData.drone2)
            {
                drone2 = drone.drone;
            }
        }
    }

    public Drone GetDrone1()
    {
        return drone1;
    }
    public Drone GetDrone2()
    {
        return drone2;
    }

}
