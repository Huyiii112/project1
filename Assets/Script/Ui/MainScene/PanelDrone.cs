﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelDrone : MonoBehaviour
{
    public Button btnNext, btnPrevious, btnBack, buttonAdd;
    public Image dronePrefabs;
    int currentIndex;

    [SerializeField]
    private List<DroneSO> m_listDrone = new List<DroneSO>();
    [SerializeField]
    private List<DroneSO> m_currentListDrone = new List<DroneSO>();
    void Start()
    {
        btnNext.onClick.AddListener(() => { Next(); });
        btnBack.onClick.AddListener(() => { Back(); });
        btnPrevious.onClick.AddListener(() => { Previous(); });
        buttonAdd.onClick.AddListener(() => { AddDrone(); });

        currentIndex = 0;
        foreach (var a in m_listDrone)
        {
            if (a.isBuy == true)
            {
                m_currentListDrone.Add(a);
            }
        }
        if (IsDrone()) dronePrefabs.sprite = m_currentListDrone[currentIndex].icon;
    }
    // Update is called once per frame
    void Update()
    {
        CheckDrone();

    }
    public void Next()
    {
        if (IsDrone())
        {
            currentIndex++;
            if (currentIndex >= m_currentListDrone.Count) currentIndex = 0;
            dronePrefabs.sprite = m_currentListDrone[currentIndex].icon;
  
        }
    }
    public void Previous()
    {
        if (IsDrone())
        {
            currentIndex--;
            if (currentIndex < 0) currentIndex = m_currentListDrone.Count - 1;
            dronePrefabs.sprite = m_currentListDrone[currentIndex].icon;
            
        }
    }
    public void Back()
    {
        MainSceneCtrl.Instance.EnablePanelDrone(false);

    }
    public void AddDrone()
    {
        MainSceneCtrl.Instance.SetDrone(m_currentListDrone[currentIndex]);
    }
    public void CheckDrone()
    {

        if (IsDrone())
        {

            if (m_currentListDrone[currentIndex] == GameData.drone1 || m_currentListDrone[currentIndex] == GameData.drone2)
            {
                buttonAdd.interactable = false;
            }
            else
            {
                buttonAdd.interactable = true;
            }

        }

    }
    public bool IsDrone()
    {

        if (m_currentListDrone.Count > 0) return true;
        else return false;

    }
}
