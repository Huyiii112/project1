using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainSceneCtrl : MonoBehaviour
{
    
    public  GameObject  panelSetting,panelSelectLv,panelSelectPlayer,panelShop,panelDrone;
    
    public PanelMain panelMain;
    public static MainSceneCtrl Instance;
    void Awake()
    {
        Instance = this;
        //GameSave.LoadDrones();
        
    }
    public void EnablePanelMenu(bool hide)
    {
        panelMain.gameObject.SetActive(hide);
    }

    public void EnablePanelSetting(bool hide)
    {
        panelSetting.SetActive(hide);
    }
    public void EnablePanelSelectPlayer(bool hide)
    {
        panelSelectPlayer.SetActive(hide);
    }
    public void EnablePanelSelectShop(bool hide)
    {
        panelShop.SetActive(hide);
    }
    public void EnablePanelSelectLv(bool hide)
    {
        panelSelectLv.SetActive(hide);
    }
    public void EnablePanelDrone(bool hide)
    {
        panelDrone.SetActive(hide);
    }
    public void SetDrone(DroneSO drone)
    {
        panelSelectPlayer.GetComponent<PanelSelected>().SetDrone(drone);
    }
    public void SetGoldMainMenu(int set)
    {
        panelMain.SetGold(set);
    }
}
