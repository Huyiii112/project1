using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class PanelSelected : MonoBehaviour
{
    public Button btnNext,btnPrevious,btnBack,drone1,drone2;
    public Image playerPrefabs;
    public Button btnClick;
    int i;

    [SerializeField]
    private List<SpriteRenderer> m_listPlayer = new List<SpriteRenderer>();
    private Sprite m_spriteDrone;

    void Start()
    {
        btnNext.onClick.AddListener(()=>{Next();});
        btnBack.onClick.AddListener(()=>{Back();});
        btnPrevious.onClick.AddListener(()=>{Previous();});
        drone1.onClick.AddListener(() => { AddDrone(drone1);});
        drone2.onClick.AddListener(() => { AddDrone(drone2); });
        i = 0;
        playerPrefabs.sprite = m_listPlayer[0].sprite;
        GameSave.SetPlayerSelected(i);
    }

    public void Next()
    {
        i++;
        if(i >= m_listPlayer.Count)    i =0;
        playerPrefabs.sprite = m_listPlayer[i].sprite;
        GameSave.SetPlayerSelected(i);
        
    }
    public void Previous()
    {
        i--;
        if(i <0)    i = m_listPlayer.Count-1;
        playerPrefabs.sprite = m_listPlayer[i].sprite;
        GameSave.SetPlayerSelected(i);
        
    }
    public void Back()
    {
        GameData.levelPlay--;
        MainSceneCtrl.Instance.EnablePanelSelectPlayer(false);
        MainSceneCtrl.Instance.EnablePanelMenu(true);
        
    }
    public void AddDrone(Button button)
    {
        MainSceneCtrl.Instance.EnablePanelDrone(true);
        btnClick = button;
    }


    public void SetDrone(DroneSO drone)
    {
        m_spriteDrone = drone.icon;
       if(btnClick == drone1)
        {
            drone1.image.sprite = m_spriteDrone;
            GameData.drone1 = drone;
        }
        else if(btnClick == drone2)
        {
            drone2.image.sprite = m_spriteDrone;
            GameData.drone2 = drone;
        }

    }
}
