using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PanelMain : MonoBehaviour
{
    public Button buttonPlay, buttonSetting, buttonShop;
    public TextMeshProUGUI txtGold;
    // Start is called before the first frame update
    void Start()
    {
        GameData.levelPlay = 0;
        buttonShop.onClick.AddListener(() => {EnableShop();});
        buttonPlay.onClick.AddListener(() => { PlayGame();});
        txtGold.text = GameSave.GetGold().ToString();
        GameData.gold = GameSave.GetGold();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EnableShop()
    {
        MainSceneCtrl.Instance.EnablePanelSelectShop(true);
    }
    public void PlayGame()
    {
        GameData.levelPlay++;
        if (GameData.levelPlay == 1)
        {
            MainSceneCtrl.Instance.EnablePanelSelectPlayer(true);
        }
        if (GameData.levelPlay == 2)
        {
            MainSceneCtrl.Instance.EnablePanelSelectLv(true);
            MainSceneCtrl.Instance.EnablePanelSelectPlayer(false);
            MainSceneCtrl.Instance.EnablePanelMenu(false);
        }
        if (GameData.levelPlay > 2)
        {
            GameData.levelPlay = 0;
        }
        
    }
    public void SetGold(int gold)
    {
        this.txtGold.text = gold.ToString();
    }
}
