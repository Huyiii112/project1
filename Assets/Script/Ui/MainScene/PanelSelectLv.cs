using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PanelSelectLv : MonoBehaviour
{
    public Button[] lvButton;
    public Button btnBack;

    void Awake()
    {
        for (int i = 1; i <= lvButton.Length - 1; i++)
        {
            lvButton[i].interactable = false;
        }
    }
    void Start()
    {
        for (int i = 0; i < GameSave.GetLastLevel(); i++)
        {
        
            lvButton[i].interactable = true;
        }
        foreach(var button in lvButton)
        {
            button.onClick.AddListener(()=>{LoadLevel(button);});
        }
        btnBack.onClick.AddListener(() => { Back(); });
    }
    public void LoadLevel(Button button)
    {

        TextMeshProUGUI buttonText = button.GetComponentInChildren<TextMeshProUGUI>();
        string textLv = buttonText.text;
        int lv = int.Parse(textLv);
        GameSave.SetLvSelected(lv-1);
        LoadScene();
    }

    public void LoadScene()
    {
        LoadingScene.LoadScene(GameConstant.SceneName.PLAY);
    }
    public void Back()
    {
        GameData.levelPlay--;
        MainSceneCtrl.Instance.EnablePanelSelectLv(false);
        MainSceneCtrl.Instance.EnablePanelSelectPlayer(true);
        MainSceneCtrl.Instance.EnablePanelMenu(true);
    }

}
