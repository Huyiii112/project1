﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PanelShop : MonoBehaviour
{
    public Button btnNext, btnPrevious, btnBack, buttonBuy, buttonItem, buttonDrone;
    public Image dronePrefabs;
    public GameObject boxDrone;
    public TextMeshProUGUI nameItem, describe, parameter, price,massage;

    [SerializeField]
    private List<DroneSO> m_listDrone = new List<DroneSO>();
    [SerializeField]
    private List<ItemSO> m_listItem = new List<ItemSO>();
    int currentIndexDrone;
    int currentIndexItem;
    [SerializeField]
    private bool m_check;
    void Start()
    {
        btnNext.onClick.AddListener(() => { Next(); });
        btnBack.onClick.AddListener(() => { Back(); });
        btnPrevious.onClick.AddListener(() => { Previous(); });
        buttonBuy.onClick.AddListener(() => { Buy(); });
        buttonItem.onClick.AddListener(() => { SelectShopItem(); });
        buttonDrone.onClick.AddListener(() => { SelectShopDrone(); });

        currentIndexDrone = 0;
        currentIndexItem = 0;
        m_check = true;
        boxDrone.SetActive(true);
        if (m_listDrone != null)
        {
            nameItem.text = m_listDrone[0].name;
            describe.text = m_listDrone[0].describe;
            parameter.text = m_listDrone[0].parameter;
            price.text = m_listDrone[0].price.ToString();
            dronePrefabs.sprite = m_listDrone[currentIndexDrone].icon;
        }

    }
    // Update is called once per frame
    void Update()
    {
        bool anyDroneBought = false;

        if (m_listDrone[currentIndexDrone].isBuy && m_check)
        {
            anyDroneBought = true;
        }
        buttonBuy.interactable = !anyDroneBought;

    }
    public void Next()
    {
        if (m_check)
        {
            currentIndexDrone++;
            if (currentIndexDrone >= m_listDrone.Count) currentIndexDrone = 0;
            nameItem.text = m_listDrone[currentIndexDrone].name;
            describe.text = m_listDrone[currentIndexDrone].describe;
            parameter.text = m_listDrone[currentIndexDrone].parameter;
            price.text = m_listDrone[currentIndexDrone].price.ToString();
            dronePrefabs.sprite = m_listDrone[currentIndexDrone].icon;

        }
        else
        {
            currentIndexItem++;
            if (currentIndexItem >= m_listItem.Count) currentIndexItem = 0;
            nameItem.text = m_listItem[currentIndexItem].name;
            describe.text = m_listItem[currentIndexItem].describe;
            price.text = m_listItem[currentIndexItem].price.ToString();
            dronePrefabs.sprite = m_listItem[currentIndexItem].icon;
        }

    }
    public void Previous()
    {
        if (m_check)
        {
            currentIndexDrone--;
            if (currentIndexDrone < 0) currentIndexDrone = m_listDrone.Count - 1;
            nameItem.text = m_listDrone[currentIndexDrone].name;
            describe.text = m_listDrone[currentIndexDrone].describe;
            parameter.text = m_listDrone[currentIndexDrone].parameter;
            price.text = m_listDrone[currentIndexDrone].price.ToString();
            dronePrefabs.sprite = m_listDrone[currentIndexDrone].icon;
        }
        else
        {
            currentIndexItem--;
            if (currentIndexItem < 0) currentIndexItem = m_listItem.Count - 1;
            nameItem.text = m_listItem[currentIndexItem].name;
            describe.text = m_listItem[currentIndexItem].describe;
            price.text = m_listItem[currentIndexItem].price.ToString();
            dronePrefabs.sprite = m_listItem[currentIndexItem].icon;
        }
    }
    public void Back()
    {
        MainSceneCtrl.Instance.EnablePanelSelectShop(false);

    }
    public void Buy()
    {

        if (m_check)
        {
            if (GameData.gold >= m_listDrone[currentIndexDrone].price)
            {
                GameData.gold -= m_listDrone[currentIndexDrone].price;
                GameSave.SetGold(GameData.gold);
                m_listDrone[currentIndexDrone].isBuy = true;
                MainSceneCtrl.Instance.SetGoldMainMenu(GameData.gold);
                massage.text = "Đã mua thành công";

            }
            else
            {
                massage.text = "Bạn không đủ tiền!";
            }
        }
        else
        {
            if (GameData.gold >= m_listItem[currentIndexItem].price)
            {
                GameData.gold -= m_listItem[currentIndexItem].price;
                GameSave.SetGold(GameData.gold);
                MainSceneCtrl.Instance.SetGoldMainMenu(GameData.gold);
                GameData.QuantityRocketItem++;
                GameSave.SetQuantityRocket();
                massage.text = "Đã mua thành công";

            }
            else
            {
                massage.text = "Bạn không đủ tiền!";
            }
        }

    }
    public void SelectShopDrone()
    {
        m_check = true;
        boxDrone.SetActive(true);
        if (m_listDrone != null)
        {
            nameItem.text = m_listDrone[0].name;
            describe.text = m_listDrone[0].describe;
            parameter.text = m_listDrone[0].parameter;
            price.text = m_listDrone[0].price.ToString();
            dronePrefabs.sprite = m_listDrone[currentIndexDrone].icon;
        }
    }
    public void SelectShopItem()
    {
        m_check = false;
        boxDrone.SetActive(false);
        if (m_listItem != null)
        {
            nameItem.text = m_listItem[0].name;
            describe.text = m_listItem[0].describe;
            price.text = m_listItem[0].price.ToString();
            dronePrefabs.sprite = m_listItem[0].icon;
        }
    }
}
