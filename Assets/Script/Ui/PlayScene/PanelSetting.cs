using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PanelSetting : MonoBehaviour
{
    public Button btnResume, btnExitMenu, btnOption, btnExitOpttion;
    public GameObject Setting;
    [SerializeField]
    private Slider m_volumeSlider;
    private void Start()
    {
        btnResume.onClick.AddListener(() => { ResumeScene(); });
        btnExitMenu.onClick.AddListener(() => { ExitMenuGame(); });
        btnOption.onClick.AddListener(() => { Option(true); });
        btnExitOpttion.onClick.AddListener(() => { Option(false); });
        m_volumeSlider.onValueChanged.AddListener(delegate {ChangeVolume();});
        m_volumeSlider.value = PlayerPrefs.GetFloat(GameConstant.Data.VolumeSfx);
        GameController.Instance.soundController.audioSourceFX.volume = m_volumeSlider.value;

    }
    public void Show(bool isShow)
    {
        if (isShow)
            LoadSetting();
        else
            ResumeScene();
    }

    public void LoadSetting()
    {
        GameController.Instance.soundController.PlaySFX(GameController.Instance.soundController.ButtonClick);
        GameController.Instance.canMove = false;
        Time.timeScale = 0;
        Setting.SetActive(true);
    }
    public void ResumeScene()
    {
        Setting.SetActive(false);
        GameController.Instance.canMove = true;
        Time.timeScale = 1;
    }
    public void ExitMenuGame()
    {
        GameController.Instance.soundController.PlaySFX(GameController.Instance.soundController.ButtonClick);
        LoadingScene.LoadScene(GameConstant.SceneName.MENU);
        GameController.Instance.canMove = true;
        Time.timeScale = 1;
        GameData.drone1 = null;
        GameData.drone2 = null;
    }

    public void Option(bool isAcctive)
    {
        GameController.Instance.uIInGame.ShowOption(isAcctive);
        Setting.SetActive(!isAcctive);
    }
    public void ChangeVolume()
    {
        GameController.Instance.soundController.audioSourceFX.volume = m_volumeSlider.value;
        PlayerPrefs.SetFloat(GameConstant.Data.VolumeSfx,m_volumeSlider.value);
    }
}
