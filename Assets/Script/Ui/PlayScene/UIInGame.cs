using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIInGame : MonoBehaviour
{
    [Header("Button")]
    public Button btnSetting;

    [Header("Panel")]
    [SerializeField]
    private PanelSetting panelSetting;
    [SerializeField]
    private GameObject panelSettingMenu;
    [SerializeField]
    private GameObject PanelHelpItem;



    [Header("UI Text")]
    [SerializeField]
    private TextMeshProUGUI waveName;
    [SerializeField]
    private TextMeshProUGUI textScore;
    [SerializeField]
    private TextMeshProUGUI leverName;
    [SerializeField]
    private TextMeshProUGUI txt_hightScore;
    [SerializeField]
    private GameObject panelEndGame;
    [SerializeField]
    private GameObject panelInGame;
    [SerializeField]
    private GameObject[] m_hpPlayer;
    [SerializeField]
    private GameObject Shield;
    [SerializeField]
    private TextMeshProUGUI m_gold;

    [SerializeField]
    private GameObject panelWinGame;
    void Start()
    {
        btnSetting.onClick.AddListener(() => { ShowSetting(true); });
        textScore.text = "Score" + "0";
    }
    void Update()
    {
        SetHightScore();
    }
    public void SetWaveName(string name)
    {
        this.waveName.text = name + " Start";
    }
    public void HideWaveName(bool hide)
    {
        waveName.gameObject.SetActive(hide);
    }

    public void SetHightScore()
    {

        txt_hightScore.text = "Hight Score: " + GameController.Instance.GetHightScoreCtrl().ToString();
    }
    public void SetScore(int score)
    {
        textScore.text = "Score: " + score.ToString();
    }
    public void HidePanelEndGame(bool hide)
    {
        panelEndGame.SetActive(hide);
    }
    public void HidePanelInGame(bool hide)
    {
        panelInGame.SetActive(hide);
    }

    public void UIHpPlayer(int hp)
    {

        for (int i = 0; i < m_hpPlayer.Length; i++)
        {
            m_hpPlayer[i].SetActive(i < hp);
        }
    }
    public void SetLvName(string name)
    {
        leverName.text = name;
    }
    public void SetGold(int gold)
    {
        m_gold.text = gold.ToString();
    }
    public void ShowWin()
    {
        HidePanelInGame(false);
        HidePanelEndGame(false);
        panelWinGame.SetActive(true);
    }
    public void SetShield(GameObject gameObject)
    {
        this.Shield = gameObject;
    }
    public void ShowShield(bool hide)
    {
        Shield.SetActive(hide);
    }
    public void ShowSetting(bool isActive)
    {
        panelSetting.Show(isActive);
    }
    public void ShowOption(bool isActive)
    {
        panelSettingMenu.SetActive(isActive);
    }
    public void ShowHelpItem(bool isActive)
    {
        PanelHelpItem.SetActive(isActive);
    }
}