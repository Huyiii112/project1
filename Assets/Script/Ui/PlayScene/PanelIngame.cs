using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PanelIngame : MonoBehaviour
{
    [SerializeField]
    private List<ItemSO> m_itemShops = new List<ItemSO>();
    public Button buttonItem;
    public TextMeshProUGUI quantity;
    private GameObject m_player;
    void Start()
    {
        m_player = GameObject.FindGameObjectWithTag("Player");
        buttonItem.onClick.AddListener(() => { FireRocKet(); });
        buttonItem.image.sprite = m_itemShops[0].icon;

        quantity.text = GameData.QuantityRocketItem.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void FireRocKet()
    {
        if (GameData.QuantityRocketItem >= 1)
        {
            Instantiate(m_itemShops[0].item, m_player.transform.position, Quaternion.identity);
            GameData.QuantityRocketItem--;
            GameSave.SetQuantityRocket();
            UpdateQuantity();

        }    
    }
    public void UpdateQuantity()
    {
        quantity.text = GameData.QuantityRocketItem.ToString();
    }    
}
