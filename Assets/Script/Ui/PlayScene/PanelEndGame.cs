using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelEndGame : MonoBehaviour
{
    public Button btnReplay, btnMenu;
    void Start()
    {
        btnReplay.onClick.AddListener(() => { RePlayGame(); });
        btnMenu.onClick.AddListener(() => { ExitMenu(); });
    }

    public void RePlayGame()
    {
        GameController.Instance.soundController.PlaySFX(GameController.Instance.soundController.ButtonClick);
        LoadingScene.LoadScene(GameConstant.SceneName.PLAY);
    }
    public void ExitMenu()
    {
        GameController.Instance.soundController.PlaySFX(GameController.Instance.soundController.ButtonClick);
       
        LoadingScene.LoadScene(GameConstant.SceneName.MENU);
    }
}
