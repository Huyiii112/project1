using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelWinGame : MonoBehaviour
{
    public Button btnMenu,btnSelectLv;

    void Start()
    {
        btnMenu.onClick.AddListener(()=>{LoadSceneMenu();});
        btnSelectLv.onClick.AddListener(()=>{ReSelectLv();});
    }
    public void LoadSceneMenu()
    {
        LoadingScene.LoadScene(GameConstant.SceneName.MENU);
    }
    public void ReSelectLv()
    {
       GameData.isNeedShowLVSelect = true;
       LoadingScene.LoadScene(GameConstant.SceneName.MAIN);
        GameData.drone1 = null;
        GameData.drone2 = null;
    }
}
