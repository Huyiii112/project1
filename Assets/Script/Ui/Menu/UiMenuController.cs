using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiMenuController : MonoBehaviour
{
    [SerializeField]
    private GameObject panelMenu, panelSetting;

    public static UiMenuController Instance;
    void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        if (GameData.isNeedShowLVSelect)
        {
           
            EnablePanelMenu(false);
            GameData.isNeedShowLVSelect = false;
        }
        else
        {
            EnablePanelMenu(true);
        }
    }
    public void EnablePanelMenu(bool hide)
    {
        panelMenu.SetActive(hide);
    }

    public void EnablePanelSetting(bool hide)
    {
        panelSetting.SetActive(hide);
    }
}
