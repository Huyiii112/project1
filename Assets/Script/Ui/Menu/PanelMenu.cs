using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelMenu : MonoBehaviour
{
    public Button btnPlay, btnExitGame, btnSetting;
    public void Start()
    {
        btnPlay.onClick.AddListener(() => { PlayGame(); });
        btnExitGame.onClick.AddListener(() => { ExitGame(); });
        btnSetting.onClick.AddListener(() => { Setting(); });
    }

    public void PlayGame()
    {
        LoadingScene.LoadScene(GameConstant.SceneName.MAIN);
    }
    public void Setting()
    {
        UiMenuController.Instance.EnablePanelSetting(true);
    }
    public void ExitGame()
    {

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }
}
