using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelSettingMenu : MonoBehaviour
{
    [SerializeField]
    private Slider m_volumeSlider;

    public AudioSource audioSourceMusic;
    public Button btnExit;

    private void Start() {
        m_volumeSlider.value = PlayerPrefs.GetFloat(GameConstant.Data.VolumeMusic,1);
        audioSourceMusic.volume = m_volumeSlider.value;
        m_volumeSlider.onValueChanged.AddListener(delegate {ChangeVolume();});
        btnExit.onClick.AddListener(()=>{ExitMenu();});
    }
    public void ChangeVolume()
    {
        audioSourceMusic.volume = m_volumeSlider.value;
        PlayerPrefs.SetFloat(GameConstant.Data.VolumeMusic,m_volumeSlider.value);
    }
    public void ExitMenu()
    {
        UiMenuController.Instance.EnablePanelSetting(false);
    }

}
