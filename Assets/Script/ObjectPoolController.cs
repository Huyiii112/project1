using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPool
{

    public List<GameObject> pooledObject;
    public int amountToPool = 20;
    public GameObject objectPrefabs;
    public ObjectType objectType;

}

public class ObjectPoolController : MonoBehaviour
{
    public static ObjectPoolController Instance;
    public List<ObjectPool> ObjectPools;
    public void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        foreach (var pool in ObjectPools)
        {
            pool.pooledObject = new List<GameObject>();

            for (int i = 0; i < pool.amountToPool; i++)
            {
                GameObject obj = Instantiate(pool.objectPrefabs);
                obj.SetActive(false);
                pool.pooledObject.Add(obj);
            }
        }
    }

    public GameObject GetObject(ObjectType objectType)
    {
        foreach (var pool in ObjectPools)
        {
            if (pool.objectType == objectType)
            {
                foreach (var obj in pool.pooledObject)
                {
                    if (!obj.activeInHierarchy)
                    {
                        obj.SetActive(true);
                        return obj;
                    }
                }
                GameObject newObj = Instantiate(pool.objectPrefabs);
                pool.pooledObject.Add(newObj);
                return newObj;
            }

        }
        return null;
    }

}
