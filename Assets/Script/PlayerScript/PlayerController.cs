using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float m_speedShotting;
    [SerializeField]
    private GameObject startPoint;

    public Collider2D player;
    public static PlayerController instance;
    public GameObject shield;

    [SerializeField]
    private List<GameObject> Player = new List<GameObject>();
    private void Awake()
    {
        GameObject playerScene = Instantiate(Player[GameSave.GetPlayerSelected()],
                                    startPoint.transform.position, quaternion.identity);

        playerScene.transform.parent = this.gameObject.transform;
        instance = this;
        m_speedShotting = 0.4f;
        shield = playerScene.transform.Find("Shield").gameObject;

    }
    private void Start()
    {
        player = GetComponentInChildren<Collider2D>();
        if (player != null) return;
    }
    #region OP
    public void IsTringger(bool isTrigger)
    {
        player.isTrigger = isTrigger;
    }
    public void ChangeSpeedShotting(float add)
    {
        if (m_speedShotting <= 0.25f) return;
        m_speedShotting -= add;

    }
    public float GetSpeedShotting()
    {
        return m_speedShotting;
    }
    #endregion

}
