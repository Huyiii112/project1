using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotting : MonoBehaviour
{
    // Start is called before the first frame update
    private float m_delayShotting;
    private float m_speedShotting;

    public ObjectType objectType;
    [SerializeField]
    private Transform[] m_pointBullets;
    void Start()
    {
        SelectPointSpawn();
    }

    // Update is called once per frame
    void Update()
    {
        m_speedShotting = PlayerController.instance.GetSpeedShotting();
        m_delayShotting += Time.deltaTime;
        if (m_delayShotting >= m_speedShotting)
        {
            SelectPointSpawn();
            m_delayShotting = 0;
        }
    }
    public void SelectPointSpawn()
    {
        GameController.Instance.soundController.PlaySFX(GameController.Instance.soundController.sfxBullet);
        int lvPower = GameController.Instance.SelectLvPower();
        switch (lvPower)
        {

            case 1:
                {
                    SpawnBullet(m_pointBullets[0]);
                    break;
                }

            case 2:
                {
                    SpawnBullet(m_pointBullets[1]);
                    SpawnBullet(m_pointBullets[2]);
                    break;
                }
            case 3:
                {
                    SpawnBullet(m_pointBullets[0]);
                    SpawnBullet(m_pointBullets[1]);
                    SpawnBullet(m_pointBullets[2]);
                    break;
                }
        }
    }
    public void SpawnBullet(Transform point)
    {
        
        GameObject bullet = ObjectPoolController.Instance.GetObject(objectType);
        if (bullet != null)
        {
            bullet.transform.position = point.position;
        }
    }

}
