using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserMovement : MonoBehaviour
{

    public float speed;
    public int x ,y;
    void Update()
    {
        transform.position += new Vector3(x,y,0)*2f*Time.deltaTime;
        if(transform.position.y >= 5.465f)
        {
            gameObject.SetActive(false);
        }
    }
    
}
