﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float moveSpeed = 1;

    private float m_minX = -2.6f;
    private float m_maxX = 2.6f;
    private float m_minY = -6f;
    private float m_maxY = 5.4f;
    //private float m_rotation;
    public float m_speedRotation = 1;
    Vector3 m_newPosition;
    private Vector3 m_clickStartPosition;
    private Vector3 m_dragOffset;


    [SerializeField]
    private Transform m_pointDrone1;
    [SerializeField]
    private Transform m_pointDrone2;

    private void Start()
    {
        if(GameController.Instance.droneController.GetDrone1())
        {
            CreateDrone1();
        }
        if (GameController.Instance.droneController.GetDrone2())
        {
            CreateDrone2();
        }
    }
    // Update is called once per frame
    void Update()
    {
        //Tính khoảng cách click chuột và player
        if (Input.GetMouseButtonDown(0))
        {
            m_clickStartPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            m_dragOffset = transform.position - m_clickStartPosition;
        }
        if (Input.GetMouseButton(0) && !IsMouseOverUi() && GameController.Instance.canMove)
        {

            Vector3 currentMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            m_newPosition = currentMousePosition + m_dragOffset;
            m_newPosition = new Vector3(Mathf.Clamp(m_newPosition.x, m_minX, m_maxX), Mathf.Clamp(m_newPosition.y, m_minY, m_maxY));
            transform.position = m_newPosition;
        }

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy") || col.CompareTag("EnemyBullet"))
        {
            if (GameController.Instance.IsShield())
            {
                GameController.Instance.ShowShields(false);

            }
            else
            {
                GameController.Instance.soundController.PlaySFX(GameController.Instance.soundController.sfxEnemyDead);
                GameController.Instance.SetHpPlayer();
                GameObject explosion = Instantiate(GameController.Instance.effect.EffectExpl, transform.position, Quaternion.identity);
                Destroy(explosion, 0.5f);
                if (GameController.Instance.GetHpPlayer() == 0)
                {
                    Destroy(gameObject);
                    explosion = Instantiate(GameController.Instance.effect.EffectExpl, transform.position, Quaternion.identity);
                    Destroy(explosion, 0.5f);
                    GameController.Instance.HideEndGame(true);
                    GameController.Instance.HideInGame(false);
                }

            }
        }

    }

    public bool IsMouseOverUi()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    public void CreateDrone1()
    {
        Drone drone1 = Instantiate(GameController.Instance.droneController.drone1, m_pointDrone1.position, Quaternion.identity);
        drone1.transform.parent = gameObject.transform;
        drone1.transform.localScale = m_pointDrone1.localScale;
    }  
    public void CreateDrone2()
    {
        Drone drone2 = Instantiate(GameController.Instance.droneController.drone2, m_pointDrone2.position, Quaternion.identity);
        drone2.transform.parent = gameObject.transform;
        drone2.transform.localScale = m_pointDrone2.localScale;
    }    
}
