using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class TurretBulletScript : MonoBehaviour
{
    GameObject Player;
    public Vector3 direction ;
    public float speedBullet;

    void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    // Update is called once per frame
    void Update()
    {
        MoveTowardsPlayer();
    }
    void MoveTowardsPlayer()
    {
        if (Player)
        {
            gameObject.transform.position += direction*speedBullet*Time.deltaTime;
        }
        else
        {
            gameObject.SetActive(false);
            return;
        }
        IsActive();
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            gameObject.SetActive(false);
        }
    }
    public void IsActive()
    {
        if (gameObject.transform.position.y < -7f)
        {
            gameObject.SetActive(false);
        }
    }

}

