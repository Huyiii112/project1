using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{

    public float value;
    public string uses;
    public float DropRate;
    public ItemType itemType;
    public void Update()
    {
            transform.position += Vector3.down * 2f * Time.deltaTime;
            if (transform.position.y < -7f)
            {
                Destroy(gameObject);
            }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            switch (itemType)
            {
                case ItemType.SpeedPowerUpItem:
                    GameController.Instance.playerController.ChangeSpeedShotting(value);
                    Destroy(gameObject);
                    break;
                case ItemType.PowerUpItem:
                    GameController.Instance.ChangeLvPower(true);
                    Destroy(gameObject);
                    break;
                case ItemType.Shield:
                    GameController.Instance.ShowShields(true);
                    Destroy(gameObject);
                    break;
                case ItemType.HealingHp:
                    GameController.Instance.SetHpHealing();
                    Destroy(gameObject);
                    break;
                default:
                    break;
            }

        }
    }

}
