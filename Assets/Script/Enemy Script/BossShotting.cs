﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossShotting : MonoBehaviour
{
    [SerializeField]
    private Transform m_listPoint;
    private float m_timeDelay;
    private float m_time1, m_time2;

    /// <summary>
    /// Lấy vector toạ độ đạn bay
    /// </summary>
    public Pos[] pos; 
    private void Start()
    {
        m_timeDelay = Random.Range(2, 5);
    }
    void Update()
    {
        m_time1 += Time.deltaTime;
        m_time2 += Time.deltaTime;
        if (m_time1 > m_timeDelay)
        {
            SpawnMiniBullet();
            m_time1 = 0;
            m_timeDelay = Random.Range(2, 5);

        }
        if (m_time2 > 4f)
        {

            SpawnBulletPowerBoss();
            m_time2 = 0;
        }

    }
    public void SpawnBulletPowerBoss()
    {
        GameObject Bullet;
        if (gameObject.tag == "BossLv5")
        {
            Bullet = ObjectPoolController.Instance.GetObject(ObjectType.BossBullet);
            Bullet.transform.position = m_listPoint.position;
           
    
        }
        if (gameObject.tag == "BossLv10")
        {
            Bullet = ObjectPoolController.Instance.GetObject(ObjectType.BossBulletLv10);
            Bullet.transform.position = m_listPoint.position;
        }

    }

    public void SpawnMiniBullet()
    {
        for(int i =0;i<pos.Length;i++)
        {
            GameObject miniBullet = ObjectPoolController.Instance.GetObject(ObjectType.BossMiniBullet);
            miniBullet.transform.position = m_listPoint.position;
            miniBullet.GetComponent<BossBullet>().SetPos(pos[i].x, pos[i].y);
        }    
    }


}
