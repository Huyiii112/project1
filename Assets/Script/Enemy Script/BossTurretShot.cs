using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTurretShot : MonoBehaviour
{
    [SerializeField]
    private Transform m_pointSpawn;
    [SerializeField]
    private GameObject m_player;
    private float m_timeSpawn;
    Vector3 newRotation;
    Vector3 direction;

    private void Start()
    {
        m_player = GameObject.FindGameObjectWithTag("Player");
    }
    public void Update()
    {

        if (!m_player) return;
        if (m_player)
        {
            m_timeSpawn += Time.deltaTime;
            SpinFollowPlayer();

            if (m_timeSpawn > 3f)
            {
                SpawnTurrentBullet();
                m_timeSpawn = 0;
            }
            
        }
        
       
    }

    public void SpawnTurrentBullet()
    {
        GameObject wait = Instantiate(GameController.Instance.effect.PowerWait, m_pointSpawn.position, Quaternion.identity);
        Destroy(wait, 0.3f);
        GameObject bullet = ObjectPoolController.Instance.GetObject(ObjectType.TurretBullet);
        bullet.transform.position = m_pointSpawn.position;
        direction = m_player.transform.position - bullet.transform.position;
        bullet.GetComponent<TurretBulletScript>().direction = this.direction;
        bullet.transform.rotation = Quaternion.Euler(newRotation);

    }

    public void SpinFollowPlayer()
    {
        Vector3 direction = m_player.transform.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        newRotation = new Vector3(0, 0, angle);
        transform.rotation = Quaternion.Euler(newRotation);
    }

}
