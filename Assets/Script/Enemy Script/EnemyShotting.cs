using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShotting : MonoBehaviour
{
    [SerializeField]
    private  float m_timeDelay;

    private float m_time;
    private void Start() {
        m_timeDelay = Random.Range(3, 6);
    }
    void Update()
    {
        m_time += Time.deltaTime;
        if (m_time > m_timeDelay)
        {
            Spawn();
            m_time =0;
            m_timeDelay = Random.Range(3, 6);
        }
    }

    public void Spawn()
    {
        GameObject bulletEnemy =  ObjectPoolController.Instance.GetObject(ObjectType.BulletEnemy1);       
        bulletEnemy.transform.position = transform.position;

    }   

}