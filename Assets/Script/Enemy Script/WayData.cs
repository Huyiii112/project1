using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
[System.Serializable]
public class WayData : MonoBehaviour
{
    // Start is called before the first frame update
    public string nameWave;
    public float spawnRate;
    // public EnemyMovement[] enemy;
    public ObjectType objectType;
    public MoveData[] move;
    public PointWave pointWave;
    public int numberEnemy;
    public Transform posSpawner;
    public int HpEnemyWave;
}
