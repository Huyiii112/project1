using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Pos
{
    public int x;
    public int y;
}
public class BulletsSpread : MonoBehaviour
{
    
    public Pos[] VectorBullet;
    // Update is called once per frame
    void Update()
    {
        if(transform.position.y <= -1f)
        {
            ExplosiveBullets();
        }    
    }

    public void ExplosiveBullets()
    {
       gameObject.SetActive(false);
       for(int i = 0;i< VectorBullet.Length; i++)
        {
            GameObject bullet = ObjectPoolController.Instance.GetObject(ObjectType.BossMiniBullet);
            bullet.transform.position = transform.position;
            bullet.GetComponent<BossBullet>().SetPos(VectorBullet[i].x, VectorBullet[i].y);
           
        }    
      
    }    
}
