using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawn : MonoBehaviour
{

    public void SpawnWave(WayData WayData)
    {
        StartCoroutine(IESpawnWave(WayData));
    }
    IEnumerator IESpawnWave(WayData WayData)
    {     
       for(int i =0;i<WayData.numberEnemy;i++)
       {                 
            GameObject enemy = ObjectPoolController.Instance.GetObject(WayData.objectType);
            enemy.transform.position = WayData.posSpawner.position;
            MoveData movePrefabs = WayData.move[Random.Range(0,WayData.move.Length)];
            PointWave lastPointWave = WayData.pointWave;
            enemy.GetComponent<EnemyMovement>().lastPoint = lastPointWave.dataPointWave[i].transform;
            enemy.GetComponent<EnemyMovement>().Move = movePrefabs.pointData;
            enemy.GetComponent<EnemyMovement>().SetHpEnemy(WayData.HpEnemyWave);
            yield return new WaitForSeconds(WayData.spawnRate);  
       }  
    }


}
