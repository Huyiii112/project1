using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BossMovement : MonoBehaviour
{
    public float speed;
    public float HpEnemy;
    public Transform[] Move;
    public Transform ColiderPoint;

    [SerializeField]
    private Slider m_hpBar;
    private Transform m_Target;
    private int m_index;
    private float m_currentHpEnemy;
    

    void Start()
    {
        m_currentHpEnemy = HpEnemy;
        m_index = 0; 
        m_Target = Move[m_index];
    }
    void Update()
    {
        MoveEnemy();
        UpdateHpbar();
    }
    protected void MoveEnemy()
    {
        if ((Vector3.Distance(transform.position, m_Target.transform.position) < 0.1f))
        {
            m_index++;
            if (m_index > Move.Length - 1)
            {
                m_index = 0;

            }
                m_Target = Move[m_index];
            
        }

        transform.position = Vector3.MoveTowards(transform.position, m_Target.position, speed * Time.deltaTime);

    }
    public void SetHpEnemy(int set)
    {
        HpEnemy = set;
        m_currentHpEnemy = HpEnemy;
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Shot"))
        {
            GameObject AnimationCollider = Instantiate(GameController.Instance.effect.EffCollider, ColiderPoint.position, Quaternion.identity);
            Destroy(AnimationCollider, 0.1f);
            col.gameObject.SetActive(false);

            m_currentHpEnemy--;
            if (m_currentHpEnemy == 0)
            {
                gameObject.SetActive(false);
                m_index = 0;
                GameObject explosion = Instantiate(GameController.Instance.effect.EffectExpl, transform.position, Quaternion.identity);
                Destroy(explosion, 0.5f);
            }
        }
    }
    public void UpdateHpbar()
    {
        m_hpBar.value = m_currentHpEnemy / HpEnemy;
    }
}
