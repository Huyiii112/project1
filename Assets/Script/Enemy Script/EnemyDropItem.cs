using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class EnemyDropItem : MonoBehaviour
{
    public  List<Item> listItem = new List<Item>();
    public void DropItem(Vector3 posDrop)
    {
        DropItem(posDrop,0);
        DropItem(posDrop, 1);
        DropItem(posDrop, 2);
        DropItem(posDrop, 3);
    }


    public void DropItemWinWave()
    {
        DropItemWinWave(transform.position, Random.Range(0, listItem.Count));
    }
    private void DropItem(Vector3 posDrop,int i)
    {
        float number = Random.Range(0,1f);
        if(number <= listItem[i].DropRate)
        {
            Item itemDrop = Instantiate(listItem[i],posDrop,Quaternion.identity);
        }
        else    return;
    }

    private void DropItemWinWave(Vector3 posDrop,int i)
    {
        Item itemDrop = Instantiate(listItem[i],posDrop,Quaternion.identity);

    }

}
