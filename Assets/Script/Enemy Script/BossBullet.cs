using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class BossBullet : MonoBehaviour
{
    public float x;
    public int y;
    [SerializeField]
    private ObjectType objectType;
    public float speed;

    private void Update()
    {
        MoveBullet();
        if(transform.position.x > 4f || transform.position.x < -4f ||
            transform.position.y > 7f || transform.position.y < -7f)
        {
            gameObject.SetActive(false);
        }    
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            gameObject.SetActive(false);
        }
    }
    public void MoveBullet()
    {
        transform.position += new Vector3(x, y, 0) * speed * Time.deltaTime;   
    }
    public void SetPos(int x,int y)
    {
        this.x = x;
        this.y = y;
    }    
}
