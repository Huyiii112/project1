using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class ItemSO : ScriptableObject
{
    new public string name;
    public int price;
    public string describe;
    public Sprite icon = null;
    public ItemShop item;
    public ItemType ItemType;
}
