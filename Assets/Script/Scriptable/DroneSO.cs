using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Drone", menuName = "Drones/Drone")]
public class DroneSO : ScriptableObject
{
    new public string name;
    public string describe;
    public Sprite icon;
    public Drone drone;
    public string parameter;
    public int price;
    public bool isBuy;
}
