
public enum ItemType
{
    SpeedPowerUpItem,
    PowerUpItem,
    Shield,
    HealingHp,
    Rocket

}
public enum ObjectType
{
    BulletPlayer,
    BulletPlayer2,
    Enemy1,
    Enemy2,
    Enemy3,
    Boss,
    Boss2,
    BulletEnemy1,
    BulletEnemy2,
    BossMiniBullet,
    BossBullet,
    BossBulletLv10,
    Boss3,
    TurretBullet,
    LaserDrone
}
