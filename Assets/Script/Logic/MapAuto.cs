using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MapAuto : MonoBehaviour
{
    public float speed;
    public Transform point;
    public GameObject MapClone;
    void Update()
    {
        transform.position += Vector3.down*speed*Time.deltaTime;
        if(gameObject.transform.position.y <= -35f)
        {
            Destroy(gameObject);
        }
    }

    public void  OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("MainCamera"))
        {
           GameObject Map = Instantiate(MapClone,point.position,Quaternion.identity);
           
        }
        
    }

}
