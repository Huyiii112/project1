using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public UIInGame uIInGame;
    public EnemySpawn enemySpawn;
    public LV[] levels;
    public static GameController Instance;
    public PlayerController playerController;
    public EnemyDropItem enemyDropItem;
    public SoundController soundController;
    public Effect effect;
    public bool canMove;
    public DroneController droneController;

    private WayData m_currentWave;
    private LV m_currentLevel;
    private int m_score;
    private int m_enemyDead;
    private int m_currentLevelIndex, m_currentWaveIndex;
    private int m_lvPower;
    private int m_hpPlayer;
    private bool m_isShield;
    void Awake()
    {
        canMove = true;
        Instance = this;
        GameData.gold = GameSave.GetGold();
        uIInGame.SetGold(GameData.gold);

    }
    //Score
    public void Start()
    {
        uIInGame.ShowHelpItem(true);
        m_hpPlayer = 3;
        m_lvPower = 1;
        m_currentLevelIndex = GameSave.GetLvSelected();
        m_currentWaveIndex = 0;
        StartLevel(levels[m_currentLevelIndex]);
        uIInGame.SetShield(playerController.shield);
    }
    #region UI

    public void SetScore(int score)
    {
        m_score += score;
        if (m_score > GameSave.GetHIghtScore())
        {
            GameSave.HightSCoreSave(m_score);
        }
        GameData.gold += score;
        uIInGame.SetGold(GameData.gold);
        GameSave.SetGold(GameData.gold);

        uIInGame.SetScore(m_score);
        
    }

    public void SetWaveText(string name)
    {
        uIInGame.SetWaveName(name);
    }
    public void SetLvText(string name)
    {
        uIInGame.SetLvName(name);
    }
    public int GetHightScoreCtrl()
    {
        return GameSave.GetHIghtScore();
    }
    public void SetHpPlayer()
    {
        m_hpPlayer--;
        uIInGame.UIHpPlayer(m_hpPlayer);
        ChangeLvPower(false);
    }
    public void SetHpHealing()
    {
        m_hpPlayer++;
        if(m_hpPlayer >= 3)
        {
            m_hpPlayer = 3;
        }
        uIInGame.UIHpPlayer(m_hpPlayer);    
    }
    public int GetHpPlayer()
    {
        return m_hpPlayer;
    }

    public void HideInGame(bool hide)
    {
        uIInGame.HidePanelInGame(hide);
    }
    public void HideEndGame(bool hide)
    {
        uIInGame.HidePanelEndGame(hide);
    }
    public void HideWaveNameCtrl(bool hide)
    {
        uIInGame.HideWaveName(hide);
    }
    #endregion
    #region GameSpawn
    public void OnEnemyDead(EnemyMovement enemy)
    {
        soundController.PlaySFX(soundController.sfxEnemyDead);
        m_enemyDead++;
        SetScore(1);
        enemyDropItem.DropItem(enemy.transform.position);
        if (m_enemyDead >= m_currentWave.numberEnemy)
        {
            m_currentWaveIndex++;
            m_enemyDead = 0;
            if (m_currentWaveIndex >= m_currentLevel.waves.Length)
            {
                GameSave.SetLastLevel(levels[m_currentLevelIndex].lever + 1);
                uIInGame.ShowWin();
            }
            else
            {
                enemyDropItem.DropItemWinWave();
                StartCoroutine(StartWave(m_currentLevel.waves[m_currentWaveIndex]));
                
            }
        }
    }

    public void ShowWinGameCrl()
    {
        uIInGame.ShowWin();
    }
    public void StartLevel(LV levelData)
    {
        m_currentLevel = levelData;
        m_currentWaveIndex = 0;
        StartCoroutine(StartWave(m_currentLevel.waves[m_currentWaveIndex]));
        SetLvText(levelData.leverName);
    }

    IEnumerator StartWave(WayData WayData)
    {
        SetWaveText(WayData.nameWave);
        HideWaveNameCtrl(true);
        yield return new WaitForSeconds(5f);
        m_currentWave = WayData;
        m_enemyDead = 0;
        enemySpawn.SpawnWave(WayData);
        HideWaveNameCtrl(false);
        uIInGame.ShowHelpItem(false);
    }
    #endregion
    public void ChangeLvPower(bool isUp)
    {
        if (isUp) m_lvPower++;
        else m_lvPower--;
        if (m_lvPower <= 1)
        {
            m_lvPower = 1;
            return;
        }
        if (m_lvPower > 3) m_lvPower = 3;
    }
    public int SelectLvPower()
    {
        return m_lvPower;
    }
    public void ShowShields(bool hide)
    {
        uIInGame.ShowShield(hide);
        m_isShield = hide;
    }
    public bool IsShield()
    {
        return m_isShield;
        
    }
}
