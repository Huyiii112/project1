
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioSource audioSourceFX;
    public AudioSource audioSource;

    public AudioClip sfxBullet, sfxEnemyDead,ButtonClick;

    public void PlaySFX(AudioClip au)
    {
        audioSourceFX.PlayOneShot(au);
    }
    
}
