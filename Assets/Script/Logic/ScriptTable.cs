using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newData",menuName ="Data")]
public class ScriptTable : ScriptableObject
{
    public  List<Drone> listDrones = new List<Drone>();
    public  Drone drone1;
    public  Drone drone2;
    public  int gold;
}
