﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameSave
{
    public static void HightSCoreSave(int score)
    {
        PlayerPrefs.SetInt(GameConstant.Data.KEY_HIGHTSCORE, score);
        PlayerPrefs.Save();
    }
    public static int GetHIghtScore()
    {
        return PlayerPrefs.GetInt(GameConstant.Data.KEY_HIGHTSCORE);
    }
    public static void SetLastLevel(int LastLv)
    {
        PlayerPrefs.SetInt(GameConstant.Data.KEY_LASTLEVEL, LastLv);
        PlayerPrefs.Save();
    }
    public static int GetLastLevel()
    {
        return PlayerPrefs.GetInt(GameConstant.Data.KEY_LASTLEVEL);
    }
    public static void SetLvSelected(int lv)
    {
        PlayerPrefs.SetInt(GameConstant.Data.LEVELSELECTED, lv);
        PlayerPrefs.Save();
    }
    public static int GetLvSelected()
    {
        return PlayerPrefs.GetInt(GameConstant.Data.LEVELSELECTED);
    }

    public static void SetPlayerSelected(int set)
    {
        PlayerPrefs.SetInt(GameConstant.Data.PLAYERSELECTED, set);
        PlayerPrefs.Save();
    }
    public static int GetPlayerSelected()
    {
        return PlayerPrefs.GetInt(GameConstant.Data.PLAYERSELECTED);
    }
    public static void SetGold(int set)
    {
        PlayerPrefs.SetInt(GameConstant.Data.GOLDPLAYER, set);
        PlayerPrefs.Save();
    }
    public static int GetGold()
    {
        return PlayerPrefs.GetInt(GameConstant.Data.GOLDPLAYER);
    }
    public static void SetQuantityRocket()
    {
        PlayerPrefs.SetInt(GameConstant.Data.QUANTITYROCKETITEM, GameData.QuantityRocketItem);
        PlayerPrefs.Save();
    }
    public static int GetQuantityRocket()
    {
        return PlayerPrefs.GetInt(GameConstant.Data.QUANTITYROCKETITEM);
    }

}

