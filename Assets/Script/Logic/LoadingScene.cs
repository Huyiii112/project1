using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScene : MonoBehaviour
{
    static string nextScene = GameConstant.SceneName.MENU;
    public Slider slider;
    public TextMeshProUGUI textProgress;
    public static void LoadScene(string sceneName)
    {
        nextScene = sceneName;
        SceneManager.LoadScene(GameConstant.SceneName.LOADING);
    }

    private void Start() {
        DoLoadScene(nextScene);
    }
    public void DoLoadScene(string sceneName)
    {
        StartCoroutine(LoadLoadSceneAsync(sceneName));   
    }

    IEnumerator LoadLoadSceneAsync(string sceneName)
    {    
        AsyncOperation operation  = SceneManager.LoadSceneAsync(sceneName); 
        operation.allowSceneActivation = false;
        float timeStartLoad = Time.time;
        float timeEndLoad = 0;
        float progress = 0;
        while (operation.progress < 0.9f)
        {
            progress =Mathf.Clamp01(operation.progress/0.9f);
            slider.value = progress;
            textProgress.text = progress*100f + "%"; 
            yield return null;
        }
        progress = 1;
        slider.value = progress;
        textProgress.text = progress*100f + "%"; 
        while(timeEndLoad -timeStartLoad <=1 )
        {
            timeEndLoad =Time.time;
            yield return new WaitForEndOfFrame();
        }
        operation.allowSceneActivation = true;
    }
}
