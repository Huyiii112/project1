using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketItem : MonoBehaviour
{
    public float speed;
    public GameObject range;
    void Start()
    {

    }
    private void OnEnable()
    {
        range.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0, 1, 0) * speed * Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") || collision.CompareTag("BossLv5") || collision.CompareTag("BossLv10") || collision.CompareTag("Boss"))
        {
            range.SetActive(true);
            Destroy(gameObject,0.2f);
           
        }
    }
}
