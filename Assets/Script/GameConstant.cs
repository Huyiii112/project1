using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConstant
{
    public class SceneName
    {
        public const string LOADING = "LoadingScene";
        public const string MENU = "MenuScene";
        public const string PLAY = "PlayScene";
        public const string MAIN = "MainScene";
    }
    public class Data
    {
        public const string PLAYERSELECTED = "PlayerSelected";
        public const string KEY_LASTLEVEL = "LastLevel";
        public const string LEVELSELECTED = "LevelSelected";
        public const string VolumeMusic = "VolumeMusic";
        public const string VolumeSfx = "VolumeSfx";
        public const string KEY_HIGHTSCORE = "HIGHTSCORE";
        public const string LISTIDDRONE = "ListIdDrone";
        public const string DROVEPATH = "Assets/Data/drones.json";
        public const string GOLDPLAYER = "Gold";
        public const string QUANTITYROCKETITEM = "QuantityRocket";
    }
}
